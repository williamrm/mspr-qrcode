module.exports = {
    name: "Users",
    Login: function (query, db) {
            return db.firebase_login(query);
        //false = incorrect logins, true= success
    },
    Register: function (query, db){
        return db.firebase_register(query.email, query.firstname, query.lastname, query.password);
    },
    getUser: function (query, db){
        return db.firebase_get_user(query);
    }
}