'use strict';

const Hapi = require('@hapi/hapi');
const QRHandler = require('./qrcode/qrcode');
const Users = require('./users/users');
const Database = require('./database/database');
let fs = require('fs');

const init = async () => {
    let tls = {
        key: fs.readFileSync('minica-key.pem'),
        cert: fs.readFileSync('minica.pem')
    };
    const server = Hapi.server({
        port: 8000,
        host: 'localhost',
        tls: tls
    });

    Database.firebase_init();
    let logged = false;
    let user = "";

    server.route([
        {
            method: 'GET',
            path: '/',
            handler: async () => {
                console.log(logged);
                console.log(user);
                return ("Forbidden");
            }},
        {
            method: 'POST',
            path: '/login',
            handler: async (request, h) => {
                logged = await Users.Login(request.payload, Database);
                if (logged)
                    user = request.payload.email;
                return (logged);
            }},
        {
            method: 'POST',
            path: '/register',
            handler: (request) => {
                return Users.Register(request.payload, Database);
            }},
        {
            method: 'POST',
            path: '/disconnect',
            handler: async () => {
                logged = false;
                user = "";
                return ("Disconnected");
            }},
        {
            method: 'POST',
            path: '/qrcode',
            handler: (request) => {
                if (logged)
                    return QRHandler.uploadQrCode(user, request.payload.uuid, Database);
                else
                    return "Forbidden"
            }},
        {
            method: 'GET',
            path: '/qrcode',
            handler: async (request) => {
                if (logged)
                    return await QRHandler.getQrCodes(user, Database);
                else
                    return "Forbidden";
            }},
        {
            method: 'GET',
            path: '/user',
            handler: async (request) => {
                if (logged)
                    return await Users.getUser(user, Database);
                else
                    return "Forbidden";
            }},

    ]);

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();