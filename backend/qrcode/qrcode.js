module.exports = {
    name: "Qrcode",
    uploadQrCode: function (user, id, db) {
        return db.firebase_upload_qrCode(user, id);
        //false = expiré ou incorrect, true= succes, ajouté!
    },
    getQrCodes: function (user, db) {
        return db.firebase_get_qrCodeDetails(user);
        //false = user inexistant, true = success, affiché
    }

}