const admin = require("firebase-admin");
const serviceAccount = require('../mpsrgostyle-firebase-adminsdk-m6y1u-c5e4407abd.json');

module.exports = {
    name: "Database",

    firebase_init: function(request, h) {
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://mpsrgostyle-default-rtdb.europe-west1.firebasedatabase.app"
        });
    },

    firebase_login: async function (query) {
        try{
            const db = admin.firestore();
            const res = await db.runTransaction(async t => {
                const snapshot = await db.collection("User")
                    .where("email", "==", query.email)
                    .where("password", "==", query.password)
                    .get();
                return !snapshot.empty;
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e){
            console.log("User not logged : ", e);
        }
    },

    firebase_register: async function (email, firstname, lastname, password) {
        try {
            const db = admin.firestore();
            const res = await db.runTransaction(async t => {
                await db.collection('User').doc().set({coupons: [], email: email, firstname: firstname, lastname: lastname, password: password });
                const resExists = await db.collection('User').where("email", "==", email).get();
                return !resExists.empty;
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e){
            console.log("User not created : ", e);
        }
    },

    
    firebase_upload_qrCode: async function (user, QRid) {
        try{
            const db = admin.firestore();
            const res = await db.runTransaction(async t => {
                this.firebase_update_scanCount(QRid);
                
                let allQRID = {};
                let userUUID = [];
        
                const qrExists = await db.collection("QRCode").doc(QRid).get();
                if (!qrExists.exists)
                return false;
                
                const getUser = await db.collection("User")
                .where("email", "==", user)
                .get()
                getUser.forEach((data) => {
                    userUUID.push(data.id);
                });
                
                allQRID = await this.firebase_get_qrCodes(user);
                if (!allQRID.includes(QRid)){
                    allQRID.push(QRid);
                }else{
                    return ("Already exists");
                }
                
                const userRef = db.collection('User').doc(userUUID[0]);
                await userRef.update({
                    coupons: allQRID,
                });
                return true;
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e){
            console.log("Qrcode not uploaded : ", e);
        }
    },
    
    firebase_update_scanCount: async function (QRid){   
        try{
            const db = admin.firestore();
            const QrcodeDetails = await db.collection("QRCode").doc(QRid).get();  
            let test = QrcodeDetails.data().scanCount;    
    
            const Qrcode = await db.collection("QRCode").doc(QRid);
            await Qrcode.update({
                scanCount: ++test,
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e) {
            console.log("Scancount not updated", e);
        }
    },

    firebase_get_qrCodes: async function (user) {
        try{
            const db = admin.firestore();
            const res = await db.runTransaction(async t => {
                let tab = [];
                const snapshot = await db.collection("User")
                    .where("email", "==", user)
                    .get()
                snapshot.forEach((data) => {
                    tab.push(data.data().coupons);
                });
                return (tab[0]);
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e) {
            console.log("Qrcode not got  : ", e);
        }
    },

    firebase_get_qrCodeDetails: async function (user){
        try{
            const db = admin.firestore();
            const res = await db.runTransaction(async t => {
                allQRID = await this.firebase_get_qrCodes(user);
                let details = [];
                for (let value of allQRID){
                    const detail = await db.collection("QRCode").doc(value).get()
                    let test = detail.data();
                    details.push(test);
                }
                return details
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e){
            console.log("Details not loaded  : ", e);
        }
    },
    
    firebase_get_user: async function(user) {
        try{
            const db = admin.firestore();
            const res = await db.runTransaction(async t => {
                let tab = [];
                const snapshot = await db.collection("User")
                    .where("email", "==", user)
                    .get()
                snapshot.forEach((data) => {
                    tab.push(data.data());
                });
                return (tab[0]);
            });
            console.log('Transaction success : ', res);
            return res;
        }catch (e){
            console.log("Details not loaded  : ", e);
        }
    }

}