# MSPR-QRCode

## Démarrage Back-end

1. Ouvrir un terminal
2. Aller dans le dossier back-end
3. Taper **_npm start_** pour le démarrage

## Démarrage Front-end

1. Ouvrir un deuxième terminal
2. Aller dans le dossier front-end
3. Taper **_npm start_** pour le démarrage
4. Sur navigateur, accéder à l'URL **_http://localhost:19002/_**

## Démarrage Expo go

1. Sur le téléphone, être sur le même réseau que l'ordinateur
2. Télécharger **Expo go** puis l'ouvrir
3. Appuyer sur **Scan QR Code**
4. Scanner le QR Code présent dans le navigateur (étape 4 Front-end)

## Application (astuce)

L'application s'ouvre sur le téléphone.
1. Secouer le téléphone fait apparaître un menu
* **Show Element Inspector** permet de voir le css des éléments sur le téléphone
* **Debug Remote JS** ouvre une page sur la navigateur de l'ordinateur permettant de voir la console (faire **Ctrl⇧J**)
