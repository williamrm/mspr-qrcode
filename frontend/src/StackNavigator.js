import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {styles} from './StackNavigator.style';
import HomeScreen from './containers/homeScreen/HomeScreen';
import ScanScreen from './containers/scanScreen/ScanScreen';
import CouponScreen from './containers/couponScreen/CouponScreen';
import LoginScreen from './containers/loginScreen/LoginScreen';
import RegisterScreen from './containers/registerScreen/RegisterScreen';
import ProfileScreen from './containers/profileScreen/ProfileScreen';
import {createStackNavigator} from '@react-navigation/stack';

const Tab = createMaterialTopTabNavigator();
const User = createStackNavigator();

function StackNavigator() {

    const userNav = () => {
        return (
            <User.Navigator
                screenOptions={{
                    headerShown: false
                }}
                initialRouteName="Login"
            >
                <User.Screen name="Login" component={LoginScreen} options={{
                    animationEnabled: false,
                }}/>
                <User.Screen name="Register" component={RegisterScreen} options={{
                    animationEnabled: false,
                }}/>
                <User.Screen name="Profile" component={ProfileScreen} options={{
                    animationEnabled: false,
                }}/>
            </User.Navigator>
        )
    };

    return (
        <NavigationContainer>
            <Tab.Navigator
                tabBarOptions={{
                    activeTintColor: 'white',
                    style: styles.tab
                }}
            >
                <Tab.Screen name="Home" component={HomeScreen}/>
                <Tab.Screen name="Scan" component={ScanScreen}/>
                <Tab.Screen name="Coupon" component={CouponScreen}/>
                <Tab.Screen name="Test" component={userNav}/>
            </Tab.Navigator>
        </NavigationContainer>
    );
}

export default StackNavigator;
