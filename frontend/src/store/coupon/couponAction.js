import { COUNTER_CHANGE } from '../index';

export function changeCount(number) {
    return {
        type: COUNTER_CHANGE,
        payload: [
            {
                value: number
            }
        ]
    }
}