import { COUNTER_CHANGE } from '../index';

const initialState = {
    number: [
        {
            value: 0
        }
    ]
};

const couponReducer = (state = initialState, action) => {
    switch(action.type) {
        case COUNTER_CHANGE:
        return {
            ...state,
            number:action.payload
        };
        default:
        return state;
    }
}
export default couponReducer;