import {createStore, combineReducers} from 'redux';
import couponReducer from './coupon/couponReducer';
import userReducer from './user/userReducer'
import registerReducer from "./register/registerReducer";

const rootReducer = combineReducers({
    number: couponReducer,
    register: registerReducer,
    dataUser: userReducer
});

const configureStore = () => {
    return createStore(rootReducer);
};

export default configureStore;