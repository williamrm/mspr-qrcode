import {DATA_USER} from '../index';

export const getDataUser = dataUser => {
    return {
        type: DATA_USER,
        payload: dataUser,
    }
};
