import {DATA_USER} from '../index';

const initialState = {
    dataUser: [
        {
            token: '',
        }
    ]
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_USER:
            return {
                ...state,
                dataUser: action.payload
            };
        default:
            return state;
    }
};
export default userReducer;