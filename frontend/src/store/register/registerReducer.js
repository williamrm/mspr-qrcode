import {REGISTRATION} from '../index';

const initialState = {
    register: [
        {
            firstname: '',
            lastname: '',
            username: '',
            email: '',
            password: '',
            confirmPassword: ''
        }
    ]
};

const registerReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTRATION:
            return {
                ...state,
                register: action.payload
            };
        default:
            return state;
    }
};
export default registerReducer;