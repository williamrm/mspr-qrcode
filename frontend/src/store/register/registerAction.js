import {REGISTRATION} from '../index';

export const registration = register => {
    return {
        type: REGISTRATION,
        payload: register,
    }
};
