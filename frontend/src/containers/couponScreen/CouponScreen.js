import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { connect } from 'react-redux';
import { changeCount } from '../../store/coupon/couponAction';
import { bindActionCreators } from 'redux';

const CouponScreen = (props) => {

  const number = props.number[0];
  
  function incrementCount() {
    props.changeCount(number.value + 1)
  }

  function decrementCount() {
    props.changeCount(number - 1)
  }

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Coupon Screeeeeen</Text>
      <Button
        title="increment"
        onPress={incrementCount}
      />
      {/*<Text>{number.value}</Text>*/}
      <Button
        title="decrement"
        onPress={decrementCount}
      />
    </View>
  );
}

const mapStateToProps = state => {
  const {number} = state
  return number
};

const ActionCreators = Object.assign(
  {},
  changeCount,
);

const mapDispatchToProps = dispatch => (
  bindActionCreators({changeCount,}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(CouponScreen);