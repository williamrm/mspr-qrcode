import * as React from 'react';
import {View, Text} from 'react-native';
import {styles} from "./ProfileScreen.style";
const ProfileScreen = (props) => {

    return (
        <View style={styles.background}>
            <Text style={styles.text}>Profile</Text>
        </View>
    );
};


export default ProfileScreen;