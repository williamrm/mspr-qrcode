import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({

    background: {
        flex: 1,
        backgroundColor: '#1a3e63',
        alignItems: 'center',
        marginTop: 25
    },
    text: {
        fontSize: 18,
        color: 'white',
        marginTop: 20,
        marginBottom: 15
    }
});