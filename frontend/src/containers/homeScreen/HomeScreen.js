import * as React from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';

const HomeScreen = (props) => {

    const number = props.number.number[0];
    const register = props.register.register[0];
    const test = props;

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Home Screen</Text>
            <Text>{number.value}</Text>
            <Text>{register.firstname}</Text>
            <Text>{register.lastname}</Text>
            <Text>{register.username}</Text>
            <Text>{register.email}</Text>
            <Text>{register.password}</Text>
            <Text>{register.confirmPassword}</Text>
            <Button
                onPress={()=> {console.log(props);console.log(register); console.log(number); console.log(test)}}
                title='Oui-Oui'
            />
        </View>
    );
};

const mapStateToProps = state => {
    return {
        number: state.number,
        register: state.register
    }
};

export default connect(mapStateToProps)(HomeScreen);