import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({

    background: {
        flex: 1,
        backgroundColor: '#1a3e63',
        alignItems: 'center',
        marginTop: 25
    },
    button: {
        color: '#1a3e63',
        borderColor: '#BDBFC1'
    },
    input: {
        width: 200,
        paddingLeft: 2,
        paddingBottom: 5,
        color: 'white',
        textAlignVertical: 'bottom',
        height: 30,
        marginBottom: 24,
        fontSize: 15,
        borderBottomColor: '#BDBFC1',
        borderBottomWidth: 2
    },
    logo: {
        height: 80,
        width: 80,
        borderRadius: 40,
        overflow: "hidden",
        marginTop: 30
    },
    text: {
        fontSize: 18,
        color: 'white',
        marginTop: 20,
        marginBottom: 15
    },
    nav: {
        fontSize: 15,
        color: 'white',
        marginTop: 20,
        marginBottom: 15,
        textDecorationLine: 'underline'
    }
});