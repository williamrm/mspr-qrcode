import * as React from 'react';
import {View, Text, TextInput, Image, Button, Alert} from 'react-native';
import {styles} from "./LoginScreen.style";
import {useState, useEffect} from "react";
import {bindActionCreators} from "redux";
import {getDataUser} from "../../store/user/userAction";
import {connect} from "react-redux";

const LoginScreen = (props) => {

    const [login, setLogin] = useState('');
    const [pwd, setPwd] = useState('');
    const dataUser = props.dataUser;
    const registration = () => {
        props.navigation.navigate('Register');
    };

    useEffect(() => {
        if(dataUser[0].token) {
            props.navigation.navigate('Profile');
        }
    });

    return (
        <View style={styles.background}>
            <Image
                style={styles.logo}
                source={require('../../assets/img/huf.jpg')}/>
            <Text style={styles.text}>Connexion</Text>
            <TextInput
                style={styles.input}
                placeholder='Login'
                placeholderTextColor="#BDBFC1"
                autoCapitalize="none"
                onChangeText={(value) => setLogin(value)}
                keyboardType='email-address'
            />
            <TextInput
                style={styles.input}
                placeholder='Mot de passe'
                placeholderTextColor="#BDBFC1"
                autoCapitalize="none"
                onChangeText={(value) => setPwd(value)}
            />
            <Button
                onPress={() => Alert.alert('Login : ' + login + ', pwd : ' + pwd)}
                color='#1a3e63'
                title="Connexion"
            />
            <Text style={styles.nav} onPress={registration}>Créer un compte</Text>
        </View>
    );
};

const mapStateToProps = state => {
    const {dataUser} = state;
    return dataUser
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({getDataUser}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);