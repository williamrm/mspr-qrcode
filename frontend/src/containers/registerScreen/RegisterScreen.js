import * as React from 'react';
import {View, Text, Button, Image, TextInput, ScrollView, Keyboard} from 'react-native';
import {connect} from 'react-redux';
import {registration} from '../../store/register/registerAction';
import {bindActionCreators} from 'redux';
import {styles} from "../registerScreen/RegisterScreen.style";
import {useEffect} from "react";
import {useState} from "react";

const RegisterScreen = (props) => {

    const register = props.register;
    let [displayLogo, setDisplayLogo] = useState(true);
    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", keyboardDidHide);
        };
    }, []);

    const keyboardDidShow = () => {
        setDisplayLogo(false);
    };

    const keyboardDidHide = () => {
        setDisplayLogo(true);
    };

    const login = () => {
        props.navigation.navigate('Login');
    };

    return (
        <View style={styles.background}>
            {
                displayLogo && <Image
                    style={styles.logo}
                    source={require('../../assets/img/huf.jpg')}/>
            }
            <Text style={styles.text}>Inscription</Text>
            <ScrollView
                style={{
                    flex: 1,
                }}
            >
                <TextInput
                    style={styles.input}
                    placeholder='Prénom'
                    placeholderTextColor="#BDBFC1"
                    onChangeText={(value) => register[0].firstname = value}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Nom'
                    placeholderTextColor="#BDBFC1"
                    onChangeText={(value) => register[0].lastname = value}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Pseudo'
                    placeholderTextColor="#BDBFC1"
                    autoCapitalize="none"
                    onChangeText={(value) => register[0].username = value}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Email'
                    placeholderTextColor="#BDBFC1"
                    autoCapitalize="none"
                    keybordType='email-address'
                    onChangeText={(value) => register[0].email = value}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Mot de passe'
                    placeholderTextColor="#BDBFC1"
                    autoCapitalize="none"
                    secureTextEntry={true}
                    onChangeText={(value) => register[0].password = value}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Confirmation mot de passe'
                    placeholderTextColor="#BDBFC1"
                    autoCapitalize="none"
                    secureTextEntry={true}
                    onChangeText={(value) => register[0].confirmPassword = value}
                />
                <Button
                    onPress={() => props.registration(register)}
                    color='#1a3e63'
                    title="Connexion"
                />
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.nav} onPress={login}>Se connecter</Text>
                </View>
            </ScrollView>

        </View>
    );
};

const mapStateToProps = state => {
    const {register} = state;
    return register
};

const mapDispatchToProps = dispatch => (
    bindActionCreators({registration}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);