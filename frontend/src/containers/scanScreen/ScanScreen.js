import * as React from 'react';
import { View, Image, Button, Text, onPressLearnMore } from 'react-native';
import { styles } from './ScanScreen.style';

const ScanScreen = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Scan Screen</Text>
        <Button
          onPress={onPressLearnMore}
          title="Send"
          color="#1a3e63"/>
      </View>
    );
  }

export default ScanScreen;