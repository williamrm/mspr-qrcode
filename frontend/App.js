import 'react-native-gesture-handler';
import * as React from 'react';
import StackNavigator from './src/StackNavigator';
import { Provider } from 'react-redux';
import configureStore from './src/store/store';

const store = configureStore()

function App() {
  return (
    <Provider store = { store }>
      <StackNavigator/>
    </Provider>
  );
}

export default App;